package com.proyecto.finalbackend.service;

import com.proyecto.finalbackend.dto.RenewPasswordFirstDTO;
import com.proyecto.finalbackend.dto.RespuestaApi;
import com.proyecto.finalbackend.dto.UpdatePasswordDTO;

public interface SecurityService {

	public RespuestaApi getToken(String username, String password);

	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);

	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);

	public RespuestaApi signOut(String token);

	public RespuestaApi refreshToken(String token);
	
}
