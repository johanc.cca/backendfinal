package com.proyecto.finalbackend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.finalbackend.dao.IEncuestaDao;
import com.proyecto.finalbackend.model.Encuesta;
import com.proyecto.finalbackend.service.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	private IEncuestaDao encuestaDao;
	
	@Override
	public List<Encuesta> obtenerEncuestas() {
		return this.encuestaDao.findAll();
	}

	@Override
	public void guardarDatos(Encuesta encuesta) {
		this.encuestaDao.save(encuesta);
	}

}
