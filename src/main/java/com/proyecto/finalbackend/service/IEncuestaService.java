package com.proyecto.finalbackend.service;

import java.util.List;

import com.proyecto.finalbackend.model.Encuesta;

public interface IEncuestaService {

	public List<Encuesta> obtenerEncuestas();
	
	public void guardarDatos(Encuesta encuesta);
	
}
