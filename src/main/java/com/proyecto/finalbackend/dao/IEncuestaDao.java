package com.proyecto.finalbackend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.finalbackend.model.Encuesta;

@Repository
public interface IEncuestaDao extends JpaRepository<Encuesta, Integer> {

}
