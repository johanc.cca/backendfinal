package com.proyecto.finalbackend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "encuesta")
public class Encuesta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "nombres", nullable = false, length = 80)
	private String nombres;

	@Column(name = "apellidos", nullable = false, length = 80)
	private String apellidos;

	@Column(name = "edad", nullable = false)
	private Integer edad;

	@Column(name = "nombre_encuesta", nullable = false, length = 80)
	private String nombreEncuesta;

	@Column(name = "tipo_encuesta", nullable = false, length = 80)
	private String tipoEncuesta;

	@Column(name = "opcion_encuesta", nullable = false, length = 20)
	private String opcionEncuesta;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public String getNombreEncuesta() {
		return nombreEncuesta;
	}

	public void setNombreEncuesta(String nombreEncuesta) {
		this.nombreEncuesta = nombreEncuesta;
	}

	public String getTipoEncuesta() {
		return tipoEncuesta;
	}

	public void setTipoEncuesta(String tipoEncuesta) {
		this.tipoEncuesta = tipoEncuesta;
	}

	public String getOpcionEncuesta() {
		return opcionEncuesta;
	}

	public void setOpcionEncuesta(String opcionEncuesta) {
		this.opcionEncuesta = opcionEncuesta;
	}

}
